<%   
  HelloDynamicProductionHelper dynamicHelper = (HelloDynamicProductionHelper) helper; //call helper
  int[] snippetVar_numbersVar = dynamicHelper.getNumbers();			// will be getting the array generated in your helper 
																	// thru prefetch() method and assign it to a variable(snippetVar_numbersVar)  
																	// that can be access in your jsp.
	
  for(int snippetVar_numberVar: snippetVar_numbersVar) {				// loop every random number from the array to able 												
%>
	<p>%%numberVar%%</p><br> <%--to print it one by one. --%>
<%}%>