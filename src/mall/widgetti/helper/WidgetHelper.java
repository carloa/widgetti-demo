package mall.widgetti.helper;

import java.util.Scanner;


public class WidgetHelper
{
	private static final String HTML_SUFFIX = ".html";
	private static final String CSS_SUFFIX = ".css";
	private static final String JSP_PROD_SUFFIX = ".production.jsp";
	private static final String JAVASCRIPT_SUFFIX = ".js";

	private WidgetHelper() {
	}
	
	private static String getFormatFileName(String name, int type) {
		switch (type) {
		case 1: // HTML
			return name + HTML_SUFFIX;
		case 2: // CSS
			return name + CSS_SUFFIX;
		case 3: // JSP Production
			return name + JSP_PROD_SUFFIX;
		case 4: // Javascript
			return name + JAVASCRIPT_SUFFIX;
		}
		return "";
	}
	
	public static String getHTMLFormatFileName(String name) {
		return getFormatFileName(name, 1);
	}
	
	public static String getCSSFormatFileName(String name) {
		return getFormatFileName(name, 2);
	}
	
	public static String getJSPProdFormatFileName(String name) {
		return getFormatFileName(name, 3);
	}
	
	public static String getJavaScriptFormatFileName(String name) {
		return getFormatFileName(name, 4);
	}

	public static String getDefaultIfEmpty(String value, String defValue) {
		if (value != null && value.length() > 0) 
			return value;
		else
			return defValue;
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter Encoded EDP: ");
		String encProdId = scan.nextLine();
		String productId = "";
		for(int i=0; i<encProdId.length(); i++) {
			productId += (encProdId.charAt(i)-97);
		}   
		System.out.println("Decoded  EDP: " + productId);
	}
}