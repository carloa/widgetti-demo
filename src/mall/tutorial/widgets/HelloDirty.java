package mall.tutorial.widgets;

import com.dinaa.ui.UimData;
import com.dinaa.ui.UimHelper;

import tooltwist.wbd.CodeInserterList;
import tooltwist.wbd.WbdException;
import tooltwist.wbd.WbdGenerator;
import tooltwist.wbd.WbdRenderHelper;
import tooltwist.wbd.WbdWidget;
import tooltwist.wbd.WbdWidgetController;

public class HelloDirty extends WbdWidgetController {

		//use for defining properties
		@Override
		protected void init(WbdWidget instance) throws WbdException {
		}

		//use to display the widget when you are in ToolTwist Designer specifically, Layout, Navpoint, Webpage
		@Override
		public void renderForDesigner(WbdGenerator generator, WbdWidget instance,
				UimData ud, WbdRenderHelper rh) throws WbdException {
			rh.append("<style type=\"text/css\">" 
					+"h1{\n" 
					+"color:green;\n" 
					+"text-align:center;\n" 
					+"}\n" 
					+"</style>\n"
					+"<h1>Hello Dirty!</h1>");
		}

		//use to display the widget when you are in ToolTwist Designer particularly in the Widget Section.
		@Override
		public void renderForPreview(WbdGenerator generator, WbdWidget instance,
				UimData ud, WbdRenderHelper rh) throws WbdException {
			rh.append("<style type=\"text/css\">" 
					+"h1{\n" 
					+"color:red;\n" 
					+"text-align:center;\n" 
					+"}\n" 
					+"</style>\n"
					+"<h1>Hello Dirty!</h1>");
			
		}

		//use to display the widget for Production, and it is also used to define Production Helper
		@Override
		public void renderForJSP(WbdGenerator generator, WbdWidget instance,
				UimHelper ud, WbdRenderHelper rh) throws Exception {
			rh.append("<style type=\"text/css\">" 
					+"h1{\n" 
					+"color:blue;\n" 
					+"text-align:center;\n" 
					+"}\n" 
					+"</style>\n"
					+"<h1>Hello Dirty!</h1>");
		}

		//it returns the name of the widget in the ToolTwist Designer
		@Override
		public String getLabel(WbdWidget instance) throws WbdException {
			return "Hello Dirty";
		}

		//use to include files like css, js etc.
		@Override
		public void getCodeInserters(WbdGenerator generator, WbdWidget instance,
				UimData ud, CodeInserterList codeInserterList) throws WbdException {	
		}
}
