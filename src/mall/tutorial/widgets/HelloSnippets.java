package mall.tutorial.widgets;

import mall.widgetti.helper.WidgetHelper;
import tooltwist.wbd.CodeInserterList;
import tooltwist.wbd.CodeInsertionPosition;
import tooltwist.wbd.JavascriptCodeInserter;
import tooltwist.wbd.SnippetParam;
import tooltwist.wbd.StylesheetCodeInserter;
import tooltwist.wbd.WbdException;
import tooltwist.wbd.WbdGenerator;
import tooltwist.wbd.WbdRenderHelper;
import tooltwist.wbd.WbdStringProperty;
import tooltwist.wbd.WbdWidget;
import tooltwist.wbd.WbdWidgetController;
import tooltwist.wbd.Snippet.SnippetLocation;

import com.dinaa.ui.UimData;
import com.dinaa.ui.UimHelper;

public class HelloSnippets extends WbdWidgetController {
	private static final String WIDGET_NAME = "helloSnippets";
	//use for defining properties
	@Override
	protected void init(WbdWidget instance) throws WbdException {
		instance.defineProperty( new WbdStringProperty("message", null, "Message", "Hello Snippets"));
	}

	//use to display the widget when you are in ToolTwist Designer specifically, Layout, Navpoint, Webpage
	@Override
	public void renderForDesigner(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		rh.append(instance.getFinalProperty(generator, "message"));
	}

	//use to display the widget when you are in ToolTwist Designer particularly in the Widget Section.
	@Override
	public void renderForPreview(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		renderForDesigner(generator, instance, ud, rh);
	}

	//use to display the widget for Production, and it is also used to define Production Helper
	@Override
	public void renderForJSP(WbdGenerator generator, WbdWidget instance,
			UimHelper ud, WbdRenderHelper rh) throws Exception {
		SnippetParam[] params = {new SnippetParam("message", instance.getFinalProperty(generator,"message"))}; 
		rh.renderSnippet(generator, instance, WidgetHelper.getHTMLFormatFileName(WIDGET_NAME), params);
	}

	//it returns the name of the widget in the ToolTwist Designer
	@Override
	public String getLabel(WbdWidget instance) throws WbdException {
		return "Hello Snippets";
	}

	//use to include files like css, js etc.
	@Override
	public void getCodeInserters(WbdGenerator generator, WbdWidget instance,
			UimData ud, CodeInserterList codeInserterList) throws WbdException {
		codeInserterList.add(new StylesheetCodeInserter(generator, instance, SnippetLocation.THEMEABLE, WidgetHelper.getCSSFormatFileName(WIDGET_NAME), null, CodeInsertionPosition.HEADER));
		codeInserterList.add(new JavascriptCodeInserter(generator, instance, SnippetLocation.THEMEABLE, WidgetHelper.getJavaScriptFormatFileName(WIDGET_NAME), null, CodeInsertionPosition.BOTTOM));
	}
}