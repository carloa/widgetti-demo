package mall.tutorial.widgets;

import com.dinaa.ui.UimData;
import com.dinaa.ui.UimHelper;
import tooltwist.wbd.CodeInserterList;
import tooltwist.wbd.WbdException;
import tooltwist.wbd.WbdGenerator;
import tooltwist.wbd.WbdRenderHelper;
import tooltwist.wbd.WbdStringProperty;
import tooltwist.wbd.WbdWidget;
import tooltwist.wbd.WbdWidgetController;

public class HelloProperty extends WbdWidgetController{
	 
	@Override
	protected void init(WbdWidget instance) throws WbdException {		
		instance.defineProperty(new WbdStringProperty("name",null,"Name","yourname")); 
																				   // name - use as a key 
																				   // Name - will be the label in the designer property.
																				   // yourname - just a default value 
	}
	
	@Override
	public void renderForDesigner(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		render(generator, instance, rh);									// content will be seen in "Edit Page" of tooltwist designer
	}

	@Override
	public void renderForPreview(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		render(generator, instance, rh);									// content will be seen in "Preview" of tooltwist designer
	}

	@Override
	public String getLabel(WbdWidget instance) throws WbdException {		
		return "Hello Property";
	}

	@Override
	public void renderForJSP(WbdGenerator generator, WbdWidget instance,
			UimHelper ud, WbdRenderHelper rh) throws Exception {
		 render(generator, instance, rh);	 // content will be seen in "Test Page" of tooltwist designer	
	}	
	
	public void render(WbdGenerator generator, WbdWidget instance, WbdRenderHelper rh) throws WbdException{	
		rh.append("Hello ");
		rh.append(instance.getFinalProperty(generator, "name"));	              
    }

	@Override
	public void getCodeInserters(WbdGenerator generator, WbdWidget instance,
			UimData ud, CodeInserterList codeInserterList) throws WbdException {	
	}
}




