package mall.tutorial.widgets;

import mall.tutorial.helper.HelloDynamicProductionHelper;
import mall.widgetti.helper.WidgetHelper;
import tooltwist.wbd.CodeInserterList;
import tooltwist.wbd.PageImportCodeInserter;
import tooltwist.wbd.WbdException;
import tooltwist.wbd.WbdGenerator;
import tooltwist.wbd.WbdProductionHelper;
import tooltwist.wbd.WbdRenderHelper;
import tooltwist.wbd.WbdWidget;
import tooltwist.wbd.WbdWidgetController;

import com.dinaa.ui.UimData;
import com.dinaa.ui.UimHelper;


public class HelloDynamic extends WbdWidgetController{
	 private static final String DEFAULT_SNIPPET_NAME = "helloDynamic";
	
	 @Override
	 protected void init(WbdWidget instance) throws WbdException {
	 }

	 @Override
	 public void renderForDesigner(WbdGenerator generator, WbdWidget instance, 
			 UimData ud, WbdRenderHelper rh) throws WbdException {
		 rh.append("Render for designer");
	}   
			

	@Override
	public void renderForPreview(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		 rh.append("Render for preview");
	}

	@Override
	public String getLabel(WbdWidget instance) throws WbdException {
		return "Hello Dynamic";
	}

	@Override
	public void renderForJSP(WbdGenerator generator, WbdWidget instance,
			UimHelper ud, WbdRenderHelper rh) throws Exception {
		 render(generator, instance, ud, rh);				
	}
	

	public void render(WbdGenerator generator, WbdWidget instance, UimData ud,  WbdRenderHelper rh) throws WbdException{		
		rh.beforeProductionCode(generator, instance, null, true);
		rh.renderSnippetForProduction( generator, instance, WidgetHelper.getJSPProdFormatFileName(DEFAULT_SNIPPET_NAME));  // used to render your jsp file e.g.(helloDynamic.jsp)
		rh.afterProductionCode(generator, instance);		
	}	              
	
	
	@Override
	public void getCodeInserters(WbdGenerator generator, WbdWidget instance,
			UimData ud, CodeInserterList codeInserterList) throws WbdException {	
		codeInserterList.add(WbdProductionHelper.codeInserter(instance, HelloDynamicProductionHelper.class.getName(), null));	// use to include your helper class(HelloDynamicProductionHelper)
		codeInserterList.add(new PageImportCodeInserter(HelloDynamicProductionHelper.class.getName()));							// to access its variable you will be needing in jsp.
	}		
}
