package mall.tutorial.widgets;

import mall.tutorial.helper.HelloUserProductionHelper;
import mall.widgetti.helper.WidgetHelper;
import tooltwist.ecommerce.AutomaticUrlParametersMode;
import tooltwist.ecommerce.RoutingUIM;
import tooltwist.wbd.CodeInserterList;
import tooltwist.wbd.PageImportCodeInserter;
import tooltwist.wbd.SnippetParam;
import tooltwist.wbd.WbdException;
import tooltwist.wbd.WbdGenerator;
import tooltwist.wbd.WbdProductionHelper;
import tooltwist.wbd.WbdRenderHelper;
import tooltwist.wbd.WbdSession;
import tooltwist.wbd.WbdStringProperty;
import tooltwist.wbd.WbdWidget;
import tooltwist.wbd.WbdWidgetController;

import com.dinaa.ui.UimData;
import com.dinaa.ui.UimHelper;


public class HelloUser extends WbdWidgetController{
	 private static final String DEFAULT_SNIPPET_NAME = "helloUser"; //snippets must have same name
	
	@Override
	protected void init(WbdWidget instance) throws WbdException {
		 instance.defineProperty(new WbdStringProperty("Name",null,"Name","")); 		 
	}

	@Override
	public void renderForDesigner(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		SnippetParam[] params = { new SnippetParam("url",""),												// related to %%url%% in your html snippet			 
								  new SnippetParam("handlerId", ""),										// related to %%handlerId%% in your html snippet	
								  new SnippetParam("name", instance.getFinalProperty(generator,"Name")),	// related to %%name%% in your html snippet	
								  new SnippetParam("designStyle", "disabled")
		}; 			
		rh.renderSnippet(generator, instance, WidgetHelper.getHTMLFormatFileName(DEFAULT_SNIPPET_NAME), params);//including html snippets without value just to show its UI
	}
			

	@Override
	public void renderForPreview(WbdGenerator generator, WbdWidget instance,
			UimData ud, WbdRenderHelper rh) throws WbdException {
		renderForDesigner(generator, instance, ud, rh);
	}

	@Override
	public String getLabel(WbdWidget instance) throws WbdException {
		return "Hello User";
	}

	@Override
	public void renderForJSP(WbdGenerator generator, WbdWidget instance,
			UimHelper ud, WbdRenderHelper rh) throws Exception {
		SnippetParam[] params = { new SnippetParam("url", RoutingUIM.navpointUrl(ud, WbdSession.getNavpointId(ud.getCredentials()), AutomaticUrlParametersMode.NO_AUTOMATIC_URL_PARAMETERS)),
				new SnippetParam("handlerId", instance.getRequestHandlerId(generator, DEFAULT_SNIPPET_NAME)),                   // handlerId = %%handlerID%% in your html snippet and "instance.getRequestHandlerId(generator, DEFAULT_SNIPPET_NAME)" = including HelloUserRequestHandler.java thru helloUser.handler.xml	
				new SnippetParam("name", instance.getFinalProperty(generator,"Name")), // "name" = %%name%% in your html snippet	and "instance.getFinalProperty(generator,"Name")" = getting the value of the property(see init method)
				new SnippetParam("designStyle", "")}; 										
				
		rh.beforeProductionCode(generator, instance, params, true);	 //including JSP snippets with value to able to
		rh.renderSnippetForProduction( generator, instance, WidgetHelper.getJSPProdFormatFileName(DEFAULT_SNIPPET_NAME));		//to perform the widget functionality in the Test Page
		rh.afterProductionCode(generator, instance);		
	}
	
	@Override
	public void getCodeInserters(WbdGenerator generator, WbdWidget instance,
			UimData ud, CodeInserterList codeInserterList) throws WbdException {	
		codeInserterList.add(WbdProductionHelper.codeInserter(instance, HelloUserProductionHelper.class.getName(), null));
		codeInserterList.add(new PageImportCodeInserter(HelloUserProductionHelper.class.getName()));
	}
}