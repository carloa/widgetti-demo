package mall.tutorial.handler;

import java.io.IOException;

import javax.servlet.ServletException;

import mall.widgetti.helper.WidgetHelper;
import tooltwist.wbd.WbdCache;
import tooltwist.wbd.WbdRequestHandler;
import tooltwist.wbd.WbdSession;

import com.dinaa.DinaaException;
import com.dinaa.data.XData;
import com.dinaa.data.XNodes;
import com.dinaa.ui.UimHelper;
import com.dinaa.xpc.Xpc;

public class HelloXPCRequestHandler extends WbdRequestHandler {

	@Override
	public boolean handler(UimHelper uh, String widgetId, String method) throws DinaaException, ServletException, IOException {
		String eDP = WidgetHelper.getDefaultIfEmpty(uh.getRequestValue("edp"), ""); //assigning to a variable("eDP") the "edp" the user have entered 
		if (eDP.length()>0) {
			Xpc xpc = new Xpc(uh.getCredentials());	
			xpc.start("mall.hello.test", "select");                           		// this is used to start mall.xpc.HelloXPC.java. see config>conf-xpc>hello.edp.xpc to understand the use of "mall.hello.test","select" and how will it direct you to the HelloXPC.java
			xpc.attrib("edp", eDP);											  		// assign the edp variable as xpc attribute
			xpc.attrib("price", "0");										  		// edp, price, catalog, and store are required attribute so we have to fill it up also.
			xpc.attrib("catalog", WbdCache.getProperty("catalogID"));
			xpc.attrib("store", WbdCache.getStoreName());
			XData data = xpc.run();
				
			System.out.println(data.getXml());										// note: just to see what are the properties available 
			
			XNodes list = data.getNodes("/select/dbo.usp_prd_GetProductDetails.1"); 
			if (list.next()) {														//	once XPC have retrieve data you can now set values to the session that the helper will be accessing soon							
				WbdSession.setTemporaryValue(uh.getCredentials(), "mytestwidget.name", list.getText("Name"));									
				WbdSession.setTemporaryValue(uh.getCredentials(), "mytestwidget.msrp", list.getText("MSRP"));
				WbdSession.setTemporaryValue(uh.getCredentials(), "mytestwidget.description", list.getText("Description"));
				WbdSession.setTemporaryValue(uh.getCredentials(), "mytestwidget.dpno", list.getText("DPNo"));
			}   
		}
		return false;
		
	}  
}


