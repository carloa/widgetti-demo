package mall.tutorial.handler;

import java.io.IOException;

import javax.servlet.ServletException;

import mall.widgetti.helper.WidgetHelper;
import tooltwist.wbd.WbdRequestHandler;
import tooltwist.wbd.WbdSession;

import com.dinaa.DinaaException;
import com.dinaa.ui.UimHelper;

public class HelloUserRequestHandler extends WbdRequestHandler {

	@Override
	public boolean handler(UimHelper uh, String widgetId, String method) throws DinaaException, ServletException, IOException {
		String name = WidgetHelper.getDefaultIfEmpty(uh.getRequestValue("Name"), "");	 // assigning in a variable(e.g name) the value of the property(e.g. "Name") 
																						 // in our widget controller(e.g. HelloUser.java)
		WbdSession.setTemporaryValue(uh.getCredentials(), "widget.name", "Hello,"+name); //setting value in the session the "widget.name" as the key 
																						 //and "Hello,"+name" as its value
		return true;
	}  
}


