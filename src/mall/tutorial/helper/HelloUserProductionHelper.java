package mall.tutorial.helper;

import java.util.Properties;
import mall.widgetti.helper.WidgetHelper;
import tooltwist.wbd.WbdProductionHelper;
import tooltwist.wbd.WbdSession;
import com.dinaa.data.XData;
import com.dinaa.ui.UimData;

public class HelloUserProductionHelper extends WbdProductionHelper {
	private String name = "";
	
	public HelloUserProductionHelper(Properties params) {
		super(params);
	}

	@Override
	public XData preFetch(UimData ud) throws Exception {		
		name  = WidgetHelper.getDefaultIfEmpty(WbdSession.getTemporaryValue(ud.getCredentials(), "widget.name"), ""); // assigning the value(e.g "widget.name","Hello,+name") we set in the session thru
																													  // handler(e.g. HelloUserRequestHandler) to a String variable(e.g. name)
		
		return null;
	}

	public String getName() {																						//method will be using in the JSP snippet
		return name;
	}
}

