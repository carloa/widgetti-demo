package mall.tutorial.helper;

import java.util.Properties;
import mall.widgetti.helper.WidgetHelper;
import tooltwist.wbd.WbdProductionHelper;
import tooltwist.wbd.WbdSession;

import com.dinaa.data.XData;
import com.dinaa.ui.UimData;

public class HelloXPCProductionHelper extends WbdProductionHelper {
	private String name = "";
	private String msrp = "";
	private String description = "";
	private String dpno = "";
	
	public HelloXPCProductionHelper(Properties params) {
		super(params);
	}

	@Override
	public XData preFetch(UimData ud) throws Exception {
		name  = WidgetHelper.getDefaultIfEmpty(WbdSession.getTemporaryValue(ud.getCredentials(), "mytestwidget.name"), "");
		dpno  = WidgetHelper.getDefaultIfEmpty(WbdSession.getTemporaryValue(ud.getCredentials(), "mytestwidget.dpno"), "");
		msrp  = WidgetHelper.getDefaultIfEmpty(WbdSession.getTemporaryValue(ud.getCredentials(), "mytestwidget.msrp"), "");
		description  = WidgetHelper.getDefaultIfEmpty(WbdSession.getTemporaryValue(ud.getCredentials(), "mytestwidget.description"), "");
		return null;
	}
	
	public String getName() {
		return name;
	}

	public String getMsrp() {
		return msrp;
	}
	
	public String getDescription(){
		
		return description;
	}

	public String getDpno() {
		return dpno;
	}

	
}