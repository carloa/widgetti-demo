package mall.tutorial.helper;

import java.util.Properties;
import java.util.Random;
import tooltwist.wbd.WbdProductionHelper;
import com.dinaa.data.XData;
import com.dinaa.ui.UimData;

public class HelloDynamicProductionHelper extends WbdProductionHelper {
	private int numbers[] = new int[10];
	
	public HelloDynamicProductionHelper(Properties params) {
		super(params);
	}

	@Override
	public XData preFetch(UimData ud) throws Exception {		// method you can put your algorithm and assign value 
		System.out.println("Param: " + getParameter("test"));
		 Random randomGenerator = new Random();					// for your variable that you will be needing to
		    for (int idx = 0; idx <=10; ++idx){					// perform your task in your jsp.
		      int randomInt = randomGenerator.nextInt(100);
		      numbers[idx] = randomInt;
		 }
		return null;
	}
	
	public int[] getNumbers() {
		return numbers;
	}
}

