package mall.xpc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.dinaa.DinaaException;
import com.dinaa.data.XData;
import com.dinaa.data.XDataException;
import com.dinaa.sql.Misc;
import com.dinaa.xpc.XpcException;
import com.dinaa.xpc.XpcSecurity;

public abstract class AbstractSPCallPlugin extends AbstractConnPoolPlugin
{
	class ParamMap
	{
		String paramName = ""; 
		String attributeName = "";
		SPParamTypes paramType = SPParamTypes.INPUT;
		SPDataTypes dataType = SPDataTypes.STRING;

		public String toString()
		{
			return attributeName + " <=> " + paramName;
		}
	}
	
	private static final String SD = "|:|";
	
	protected static Logger logger = Logger.getLogger(AbstractSPCallPlugin.class);
	private static final long SP_EXECUTION_THRESHOLD = 400;
	private ArrayList<ParamMap> paramMap = new ArrayList<ParamMap>();
	private String spName = "";
	private String connName = "";
	private String cDataOutRecords = "";
	private Hashtable<String, Integer> htParamCache = null;
		
	private void createMapping(String paramName, String attributeName, SPParamTypes paramType, SPDataTypes dataType)
	{
		ParamMap map = new ParamMap();
		map.paramName = paramName;
		map.attributeName = attributeName;
		map.paramType = paramType;
		map.dataType = dataType;
		paramMap.add(map);
	}
	
	private String initSPParamPos() {
		if (paramMap.size() > 0) {
			StringBuffer paramList = new StringBuffer();
			for(Iterator<ParamMap> i = paramMap.iterator(); i.hasNext();) {
				if (!i.next().paramType.equals(SPParamTypes.RETURN)) {
					if (paramList.length() > 0) {
						paramList.append(",?");
					} else {
						paramList.append("(?");
					}
				}
			}
			if (paramList.length() > 0) paramList.append(")");
			return paramList.toString();
		}
		return "";
	}
	
	private String validateColName(String colName, int colCnt) {
		if (colName.length() <= 0)
			return "Data" + colCnt;
		else 
			return colName;
	}
	
	private long getTime(String value, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format);
		return df.parse(value).getTime();
	}	  

	private void setSPParam(CallableStatement spCStmt,ParamMap map, String value) throws DinaaException {
		try {
			if ((value.length() <= 0) &&
					(map.dataType != SPDataTypes.STRING)) {
				value = "0";
			}
			int ordPos = htParamCache.get(map.paramName.toLowerCase()); 
			            
			switch(map.dataType) {
				case BOOLEAN:
					spCStmt.setBoolean(ordPos, Boolean.parseBoolean(value));
					break;
				case DATE:
					spCStmt.setDate(ordPos, new java.sql.Date(getTime(value ,"MM/dd/yyyy")));
					break;
				case DOUBLE:
					spCStmt.setDouble(ordPos, Double.parseDouble(value));
					break;
				case FLOAT:
					spCStmt.setFloat(ordPos, Float.parseFloat(value));
					break;
				case INT:
					spCStmt.setInt(ordPos, Integer.parseInt(value));
					break;
				case LONG:
					spCStmt.setLong(ordPos, Long.parseLong(value));
					break;
				case NULL:
					spCStmt.setObject(ordPos, value.equals("0") ? null : value);
					break;
				case SHORT:
					spCStmt.setShort(ordPos, Short.parseShort(value));
					break;
				case STRING:
					spCStmt.setString(ordPos, value);
					break;
				case BYTE:
					spCStmt.setByte(ordPos, Byte.parseByte(value));
					break;
				case TIME:
					spCStmt.setTime(ordPos, new java.sql.Time(getTime(value , "h:mm:ss a")));
					break;
				case TIMESTAMP:
					spCStmt.setTimestamp(ordPos, new java.sql.Timestamp(getTime(value , "MM/dd/yyyy h:mm:ss a")));
					break;
			}
		} catch (Exception e) {
			String msg = "Error setting SP parameter value: " + e.getMessage();
			logger.error(msg,e);
			throw new XpcException(msg);
		}
	}
	
	private void regSPParam(CallableStatement spCStmt,ParamMap map) throws DinaaException {
		try {
			int ordPos = htParamCache.get(map.paramName.toLowerCase()); 		
			switch(map.dataType) {
				case BOOLEAN:
					spCStmt.registerOutParameter(ordPos, java.sql.Types.BOOLEAN);
					break;
				case DATE:
					spCStmt.registerOutParameter(ordPos, java.sql.Types.DATE);
					break;
				case DOUBLE:
					spCStmt.registerOutParameter(ordPos, java.sql.Types.DOUBLE);
					break;
				case FLOAT:
					spCStmt.registerOutParameter(ordPos, java.sql.Types.FLOAT);
					break;
				case INT:
					spCStmt.registerOutParameter(ordPos, java.sql.Types.INTEGER);
					break;
				case STRING:
					spCStmt.registerOutParameter(ordPos, java.sql.Types.VARCHAR);
					break;
				default:
					spCStmt.registerOutParameter(ordPos, java.sql.Types.JAVA_OBJECT);
					break;
			}
		} catch (Exception e) {
			String msg = "Error registering SP output parameter: " + e.getMessage();
			logger.error(msg,e);
			throw new XpcException(msg);
		}
	}
	
	private  String toUnicodeEscapeString(String str) {
	    StringBuffer buf = new StringBuffer();
	    int len = str.length();
	    char ch;
	    for (int i = 0; i < len; i++) {
	    	ch = str.charAt(i);
	    	int ci = 0xffff & ch;
	    	if (ci > 7 && ci < 127 && ci!=11)
	    		buf.append(ch);
	    	else {
	    		buf.append("&#");
	    		buf.append(new Integer(ci).toString());
	    		buf.append(';');
	    	}
	    }
	    return buf.toString();
	}
	
	private void createCDataChildElement(Element parent, String name, String value) throws XpcException
	{
		try
		{
			Document doc = parent.getOwnerDocument();

			parent.appendChild(doc.createTextNode("  "));

			Element newElem = doc.createElement(name);
			parent.appendChild(newElem);
			CDATASection cdata = doc.createCDATASection(toUnicodeEscapeString(value==null?"":value));
			//CDATASection cdata = doc.createCDATASection(toUnicodeEscapeString(value));
			newElem.appendChild(cdata);

			parent.appendChild(doc.createTextNode("\n"));
		}
		catch (org.w3c.dom.DOMException e)
		{
			logger.error(e,e);
			throw new XpcException("Exception encountered on creating CDATA element: (" + e.getMessage() + ")");
		}
	}
	
	public void mapColumn(String paramName, String attributeName, SPParamTypes paramType, SPDataTypes dataType)
	{
		createMapping(paramName, attributeName, paramType, dataType);
	}

	public void mapColumn(String paramName, String attributeName, SPDataTypes dataType)
	{
		createMapping(paramName, attributeName, SPParamTypes.INPUT, dataType);
	}
	
	public void mapColumn(String paramName, String attributeName, SPParamTypes paramType)
	{
		createMapping(paramName, attributeName, paramType, SPDataTypes.STRING);
	}
	
	public void mapColumn(String paramName, SPParamTypes paramType, SPDataTypes dataType)
	{
		createMapping(paramName, paramName, paramType, dataType);
	}
	
	public void mapColumn(String paramName, SPParamTypes paramType)
	{
		createMapping(paramName, paramName, paramType, SPDataTypes.STRING);
	}
	
	public void mapColumn(String paramName, SPDataTypes dataType)
	{
		createMapping(paramName, paramName, SPParamTypes.INPUT, dataType);
	}
	
	public void mapColumn(String paramName, String attributeName)
	{
		createMapping(paramName, attributeName, SPParamTypes.INPUT, SPDataTypes.STRING);
	}
	
	public void mapColumn(String paramName)
	{
		createMapping(paramName, paramName, SPParamTypes.INPUT, SPDataTypes.STRING);
	}
	
	public void mapCDataOutRecord(String outputName) 
	{
		cDataOutRecords += SD + outputName.toLowerCase() + SD; 
	}
	
	public void mapSP(String spName)
	{
		this.spName = spName;
	}
	
	public void mapConnection(String connName)
	{
		this.connName = connName;
	}
	
	public abstract void initialize(XpcSecurity securityDetails, XData data) throws XpcException;
	
	public XData runMethod(XpcSecurity securityDetails, Element elem) throws DinaaException
	{
		XData inputData;
		XData outputData;
		CallableStatement spCStmt=null;
		try {
			inputData = new XData(elem);
		}
		catch (XDataException e)
		{
			throw new XpcException("Error constructing document from input element: " + e.getMessage());
		}

		initialize(securityDetails, inputData);
		String xpcEntity = elem.getNodeName();		

		if (this.spName.length() <= 0) {
			// If SP name is not explicitly provided XPC Method is the name of the SP
			this.spName = elem.getAttribute("method");
		}
		
		outputData = new XData("<select/>");
				
        if (this.connName.length() <= 0) {
                int conPos = xpcEntity.indexOf(".conn.");
                if (conPos > 0) {
                    this.connName = xpcEntity.substring(conPos+6).trim();
                }              
        } 
        
		long startTime = System.currentTimeMillis();
		ResultSet rs =null;
		
		Connection jdbcConnectionLocal = null;
		if (this.connName!=null && this.connName.length()>0){ 
		    jdbcConnectionLocal=this.getConnection(connName);
		}
		
		// Check if we have a connection
		if (jdbcConnectionLocal != null) {
			try {
			    // Check if we a parameter order cache for the called stored procedure 
				String cacheKey = this.connName + "." + this.spName;
				SPParamsCache spParamCache = SPParamsCache.getInstance();
				if (spParamCache.spContainsParam(cacheKey)) 
					htParamCache = spParamCache.getSPParams(cacheKey);
				else
					htParamCache = createAndCacheSPParams(jdbcConnectionLocal,spParamCache, cacheKey);
				
				String sqlStmt = "{call " + this.spName + initSPParamPos() + "}";
				spCStmt = jdbcConnectionLocal.prepareCall(sqlStmt);
				String value = "";
				String parameterList = "";
				if (paramMap.size() > 0) {
					for(Iterator<ParamMap> i = paramMap.iterator(); i.hasNext();) {
						ParamMap map = i.next();
						switch (map.paramType) {
						case INOUT :
						case OUTPUT:
						case RETURN:
							regSPParam(spCStmt,map);
							parameterList += (parameterList.length()>0?" | ":"") + "In-Out-Return Param="+map.paramName; 
							if (map.paramType != SPParamTypes.INOUT)
								break;
						case INPUT :
							if (inputData.getNodeList("/*/"+map.attributeName).getLength()>0) {
								value = inputData.getText("/*/"+map.attributeName);
								setSPParam(spCStmt,map, value);
								parameterList += (parameterList.length()>0?" | ":"") + map.paramName + "=" + value;
							} else throw new XpcException("Attribute name not found: " + map.attributeName);
							break;
						}
					}
				}
				//logger.debug("Calling SP | " + this.spName + " | SP parameters | " + parameterList);
				Document doc = null;
				Element element = null;
				Element resultElement = null;
				boolean withResult = false;
				int resultSetCnt = 0;
				int updateOpCnt = 0;

				// An execute will return one result for each statement in the stored proc,
				// So we need to loop around through each of the results.
				doc = outputData.getDocument();
				resultElement = doc.getDocumentElement();
				for (boolean firstTime = true; ; firstTime = false)
				{
					boolean isQuery;
					int updateCount = 0;
					if (firstTime)
					{
						// Execute the query to get the first result
						isQuery = spCStmt.execute();
						updateCount = spCStmt.getUpdateCount();
					}
					else
					{
						// Get the next result
						isQuery = spCStmt.getMoreResults();
						updateCount = spCStmt.getUpdateCount();
					}
					
					// One of three things should have happened here:
					// a) We have a result that contains a resultSet (isQuery==true && updateCount is irrelevant)
					// b) We have no more results (isQuery==false && updateCount == -1)
					// c) We have a result from an update (isQuery==false && updateDount >= 0)
					
					// See if a select or an update was performed
					if (isQuery)
					{
						// A query was performed, returning a set of records
						rs = spCStmt.getResultSet();
						if (rs != null)
						{
							ResultSetMetaData dbMeta = rs.getMetaData();
							resultSetCnt++;
							// Get the result
							while (rs.next())
							{
								element = doc.createElement(this.spName + "." + resultSetCnt);
								for (int col = 1; col <= dbMeta.getColumnCount(); col++)
								{
									if (cDataOutRecords.indexOf(SD + dbMeta.getColumnName(col).toLowerCase() + SD) < 0)
									{
										String column = validateColName(dbMeta.getColumnName(col), col);
										String columnValue = rs.getString(col);					
										try{
										    Misc.createChildElement(element, column, XData.xmlFmt(columnValue));
										}catch(ArrayIndexOutOfBoundsException ae){
								            logger.error("Exception ArrayIndexOutofBounds: + " + " " + this.connName + " " + this.spName + " ("+ ae.toString() + ")");
                                            logger.error("ArrayOutofBoundsMessage: (" + columnValue + ")");
										}
									}
									else
									{
										String column = validateColName(dbMeta.getColumnName(col), col);
										String columnValue = rs.getString(col);
										createCDataChildElement(element, column, columnValue);
									}
								}
								resultElement.appendChild(element);
								resultElement.appendChild(doc.createTextNode("\n"));
								withResult = true;
							}
							try
							{
								if (rs != null)
								{
									rs.close();
									rs = null;
								}
							}
							catch (SQLException e)
							{
								rs = null;
							}
						} // rs != null
					}
					else if (updateCount < 0)
					{
						// No more results. 
						break;
					}
					else
					{
						// Was an update
						updateCount = spCStmt.getUpdateCount();
						if (updateCount > 0)
						{
							updateOpCnt++;
							element = doc.createElement("DBUpdate." + updateOpCnt);
							Misc.createChildElement(element, "NoOfUpdated", "" + updateCount);
							resultElement.appendChild(element);
							resultElement.appendChild(doc.createTextNode("\n"));
							withResult = true;
						}
					}
				}

				// Check for output parameters
				if (doc == null)
				{
					doc = outputData.getDocument();
					resultElement = doc.getDocumentElement();
				}
				boolean withOuputParam = false;
				for(Iterator<ParamMap> i = paramMap.iterator(); i.hasNext();) {
					ParamMap map = i.next();
					switch (map.paramType) {
						case INOUT :
						case OUTPUT:
						case RETURN:
							if (!withOuputParam) {
								withOuputParam = true;
								withResult = true;
								element = doc.createElement("Return");
							}
							int ordPos = htParamCache.get(map.paramName.toLowerCase());
							if (cDataOutRecords.indexOf(SD+map.paramName.toLowerCase()+SD) < 0) {
								Misc.createChildElement(element, map.paramName, XData.xmlFmt(spCStmt.getString(ordPos)));
							} else {
								createCDataChildElement(element, map.paramName, spCStmt.getString(ordPos));
							}
							
							resultElement.appendChild(element);
							resultElement.appendChild(doc.createTextNode("\n"));
							break;
					}
				}
				
				if (withResult)
					outputData.notifyDocumentChanged();
				else
					outputData = new XData("<norecordfound/>");
				
			} catch (Exception e) {
				outputData = new XData("<error>Exception encountered on running SP: ("+ e.getMessage() +")</error>");
				logger.error("Exception encountered on running SP: + " + " " + this.connName + " " + this.spName + " ("+ e.toString() + ")",e);
			}
			finally{
                 try
                 {
                    if (rs != null)
                     {
                         rs.close();
                         rs = null;
                    }
                 }
                 catch (Exception e)
                 {
                    rs = null;
                 }
                 finally{
    		         try 
    		         {
    		             if (spCStmt != null)
    		             {
    		                 spCStmt.close();
    		                 spCStmt = null;
    		             }
    		          }
    		          catch (Exception e) 
    		          {
    		             spCStmt = null;
    		          }
    		          finally{
    	                  try{
    	                      this.closeConnection(jdbcConnectionLocal);
    	                  }catch(Exception e){
    	                      jdbcConnectionLocal=null;
    	                  }
    		          }
                 }
			}
			
			logSPCallsInterval(startTime);
			//logger.debug("SP Ouput | \n"+ outputData.getXml());
		} else {
			outputData = new XData("<error>Could not connect on the database</error>");
			logger.error("Could not connect on the database");
		}								
		return outputData;
	}
	
	private void logSPCallsInterval(long startTime){
		long endTime = System.currentTimeMillis();
		long interval = endTime - startTime;	

		if(interval > SP_EXECUTION_THRESHOLD){
			logger.info("**********************************SP Execution Interval***************************");
			logger.info("SP elapsed time | " + " " + this.connName + " " + this.spName +" | "+interval+"ms");	
		}
	}
	
	private Hashtable<String, Integer> createAndCacheSPParams(Connection jdbcConnectionLocal,SPParamsCache spParamCache, String cacheKey) throws SQLException{
		Hashtable<String, Integer> htCache = new Hashtable<String, Integer>();
		Statement sqlStmt = null;
		ResultSet rs = null;
		try { 
		    sqlStmt = jdbcConnectionLocal.createStatement();
    		rs = sqlStmt.executeQuery("exec dbo.sp_sproc_columns @procedure_owner='" + getDBOwner() + "',@procedure_name='"+getProcedureName()+"'");
    		if (rs != null) {
    			while (rs.next()) {
    				String colName = rs.getString("column_name").toLowerCase();
    				int pos = colName.indexOf("@");
    				if (pos>=0)	colName = colName.substring(pos+1);
    				htCache.put(colName, rs.getInt("ordinal_position"));
    			}
    		}
    		spParamCache.addSPParams(cacheKey, htCache);
		}
		catch(SQLException sqle){
		    throw sqle;
		}
		finally{
		    try { 
    		    if (rs!=null){
    		        rs.close();
    		    }
		    }catch(Exception e){
		        rs=null;
		    }
		    try{
    		    if (sqlStmt!=null){
    		        sqlStmt.close();
    		    }
		    }
		    catch(Exception e){
		        sqlStmt=null;
		    }
		}
		return htCache;
	}
	
	private String getDBOwner() {
		int pos = this.spName.indexOf("."); 
		if (pos > 0)
			return this.spName.substring(0, pos);
		else
			return "dbo";
	}
	
	private String getProcedureName() {
		int pos = this.spName.indexOf("."); 
		if (pos > 0)
			return this.spName.substring(pos+1);
		else
			return this.spName;
	}
	

}
