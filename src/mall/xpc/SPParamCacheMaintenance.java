package mall.xpc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mall.widgetti.helper.WidgetHelper;

public class SPParamCacheMaintenance extends HttpServlet
{
	private static final long serialVersionUID = 3957492314159266139L;
	private static final String OP = "op";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		doPost(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		String operation = WidgetHelper.getDefaultIfEmpty(req.getParameter(OP), "");
		PrintWriter pw = resp.getWriter();
		pw.println("<html><head><head><body>");
		SPParamsCache spParamCache = SPParamsCache.getInstance();
		
		if (operation.equals("list")) {
			Iterator<String> cacheKeys = spParamCache.getCacheKeys();
			pw.println("<br>Current Cached SP Parameter Content(s):<br>");
			pw.println("-------------------------------------------<br>");
			while(cacheKeys.hasNext())
				pw.println("-- "+cacheKeys.next()+"<br>");
		} else if (operation.equals("clear")) {
			pw.println("<br>Current SP cache size is " + spParamCache.getCacheSize());
			spParamCache.clearCache();
			pw.println("<br>Cache cleared.<br>");
			pw.println("<br>Current SP cache size is " + spParamCache.getCacheSize());
		} else {
			pw.println("<br>Operation not supported<br>");
		}
		pw.println("</body></html>");		
	}
}
