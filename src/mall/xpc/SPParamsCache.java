package mall.xpc;

import java.util.Hashtable;
import java.util.Iterator;


public class SPParamsCache
{
	private static SPParamsCache instance = new SPParamsCache();
	private final ConcurrentCacheMap<String, Hashtable<String, Integer>> cache;

	public static SPParamsCache getInstance() {
		return instance;
	}

	private SPParamsCache()	{
		cache = new ConcurrentCacheMap<String, Hashtable<String, Integer>>(1000, 1000L * 60L * 15L, 1000L * 120L);
	}

	public ConcurrentCacheMap<String, Hashtable<String, Integer>> getCache() {
		return cache;
	}
	
	public Iterator<String> getCacheKeys() {
		return cache.keySet().iterator();
	}
	
	public boolean spContainsParam(String spName) {
		return cache.containsKey(spName);
	}
	
	public Hashtable<String, Integer> getSPParams(String spName) {
		return (Hashtable<String, Integer>) cache.get(spName);
	}

	public synchronized void addSPParams(String spName, Hashtable<String, Integer> params) {
		cache.put(spName, params);
	}
	
	public void clearCache() {
		cache.clear();
	}
	
	public int getCacheSize() {
		return cache.size();
	}
}
