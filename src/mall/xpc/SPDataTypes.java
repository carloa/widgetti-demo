package mall.xpc;

public enum SPDataTypes
{
	 BOOLEAN,
	 BYTE,
	 DATE,
	 DOUBLE,
	 FLOAT,
	 INT,
	 LONG,
	 NULL,
	 SHORT,
	 STRING,
	 TIME,
	 TIMESTAMP
}
