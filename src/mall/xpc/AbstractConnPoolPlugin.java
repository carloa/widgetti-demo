package mall.xpc;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.dinaa.DinaaException;
import com.dinaa.xpc.XpcException;
import com.dinaa.xpc.XpcMethod;

public abstract class AbstractConnPoolPlugin implements XpcMethod
{
	private static Hashtable<String, DataSource> hsDBCDetails = new Hashtable<String, DataSource>();
	
	protected Connection jdbcConnection = null; 
	static Logger logger = Logger.getLogger(AbstractConnPoolPlugin.class);
	
	private DataSource loadConnectionDetails(String connName) throws NamingException {
		// Check if details has been previously loaded
		if (hsDBCDetails.containsKey(connName)) {
			return hsDBCDetails.get(connName);
		} else {
			Context envCtx = (Context) new InitialContext().lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup(connName);
			hsDBCDetails.put(connName, ds);
			return ds;
		}
	}
	
	public String openConnectionFromEntity(String entityName) throws DinaaException
	{
		int conPos = entityName.indexOf(".conn.");
		if (conPos > 0) {
			String connName = entityName.substring(conPos+6).trim();
			return (openConnection(connName));
		}
		return "";
	};
	
	public String openConnection(String connName) throws DinaaException
	{
		try {
			// Load the connection details
			DataSource dataSource = loadConnectionDetails(connName);
			jdbcConnection = dataSource.getConnection();
			if (jdbcConnection != null) {
				return connName;
			}
		} catch (Exception e) {
			throw new XpcException("Connection Name: " + connName + "  - Error opening data connection: (" + e.getMessage() + ")");
		}
		logger.error("openConnection:JDBC connection creation unsuccessful for " + connName + " : ");
		return ""; 
	};
	
    public Connection getConnection(String connName) throws DinaaException{
        try{
            DataSource dataSource = loadConnectionDetails(connName);
            return dataSource.getConnection();
        }
        catch(Exception e){
            logger.error("openConnection:JDBC connection creation unsuccessful for " + connName + " : " + e.getMessage());            
            throw new XpcException("Connection Name: " + connName + "  - Error opening data connection: (" + e.getMessage() + ")");         
        }       
    }
    public boolean closeConnection(Connection conn) throws DinaaException{
        try {
            if (conn != null && !conn.isClosed())
            {
                conn.close();
                conn = null;
            }
        } catch (SQLException e) {
            conn = null;
            throw new XpcException("Error closing data connection: (" + e.getMessage() + ")");
        }
        return false;        
    }
    
	
	public boolean closeConnection() throws DinaaException
	{
		try	{
			if (jdbcConnection != null && !jdbcConnection.isClosed())
			{
				jdbcConnection.close();
				jdbcConnection = null;
			}
		} catch (SQLException e) {
			jdbcConnection = null;
			throw new XpcException("Error closing data connection: (" + e.getMessage() + ")");
		}
		return false;
	}
}
